<?php

namespace Container2wfhcMy;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder83769 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer3f2eb = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties64a75 = [
        
    ];

    public function getConnection()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getConnection', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getMetadataFactory', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getExpressionBuilder', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'beginTransaction', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getCache', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getCache();
    }

    public function transactional($func)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'transactional', array('func' => $func), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->transactional($func);
    }

    public function commit()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'commit', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->commit();
    }

    public function rollback()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'rollback', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getClassMetadata', array('className' => $className), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'createQuery', array('dql' => $dql), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'createNamedQuery', array('name' => $name), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'createQueryBuilder', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'flush', array('entity' => $entity), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'clear', array('entityName' => $entityName), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->clear($entityName);
    }

    public function close()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'close', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->close();
    }

    public function persist($entity)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'persist', array('entity' => $entity), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'remove', array('entity' => $entity), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'refresh', array('entity' => $entity), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'detach', array('entity' => $entity), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'merge', array('entity' => $entity), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getRepository', array('entityName' => $entityName), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'contains', array('entity' => $entity), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getEventManager', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getConfiguration', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'isOpen', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getUnitOfWork', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getProxyFactory', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'initializeObject', array('obj' => $obj), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'getFilters', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'isFiltersStateClean', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'hasFilters', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return $this->valueHolder83769->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer3f2eb = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder83769) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder83769 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder83769->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, '__get', ['name' => $name], $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        if (isset(self::$publicProperties64a75[$name])) {
            return $this->valueHolder83769->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder83769;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder83769;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder83769;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder83769;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, '__isset', array('name' => $name), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder83769;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder83769;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, '__unset', array('name' => $name), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder83769;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder83769;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, '__clone', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        $this->valueHolder83769 = clone $this->valueHolder83769;
    }

    public function __sleep()
    {
        $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, '__sleep', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;

        return array('valueHolder83769');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer3f2eb = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer3f2eb;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer3f2eb && ($this->initializer3f2eb->__invoke($valueHolder83769, $this, 'initializeProxy', array(), $this->initializer3f2eb) || 1) && $this->valueHolder83769 = $valueHolder83769;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder83769;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder83769;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
