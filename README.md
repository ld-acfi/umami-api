
<img src="/umami-api.png" height="200px" align="right"/>

# umami-api
REST API for cooks made with API Platform 

## Install

Api running with PHP 8, Symfony 5.3.

    $ git clone
    $ cd umami-api
    $ composer install
    $ php bin/console d:d:c
    $ php bin/console d:m:m
    $ symfony serve

You can now go to https://localhost


You can also visit the api, quickly deployed thanks to Heroku at this address : 
